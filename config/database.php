<?php
class Database {
  public const HOST = 'localhost';
  public const USERNAME = 'root';
  public const PASSWORD = '';
  public const DBNAME = 'tablettenbox';
  public $connection;

  public function __construct() {
    $this->connection = null;
    try {
      $this->connection = new mysqli(Database::HOST, Database::USERNAME, Database::PASSWORD, Database::DBNAME); 
    } catch (Exception $e) {
      echo $e->getMessage();
    }

    return $this;
  }

  public function __destruct() {
    $this->connection->close();
  }
}

?>
