<?php
include_once "config/database.php";

/**
 * repository is an abstraction layer to make queries to db
 */
abstract class AbstractRepository
{
  private $mysqli;  
  public $db;

  public function __construct()
  {
    $this->mysqli = new Database();
    $this->db = $this->mysqli->connection;
  }

  public function query($stmt): array
  {
    try {
      
      $stmt->execute();
      $res = $stmt->get_result();
      if (!$res || is_bool($res)) {
        return [];
      }
      return $res->fetch_all(MYSQLI_ASSOC);
    } catch (Exception $e) {
      exit($e->getMessage());
    }
  }
  
}   