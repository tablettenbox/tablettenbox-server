<?php
include_once "src/repository/EinnahmenRepository.php";
include_once "src/repository/AufzeichnungenRepository.php";
include_once "src/controller/AbstractController.php";


class EinnahmenController extends AbstractController {
  private EinnahmenRepository $einnahmenRepository;
  private ResponseFactory $responseFactory;
  private $body;

  public function __construct(string $requestMethod, array $uris) 
  {
    $body = json_decode(file_get_contents('php://input'), true);
    $this->body = $body;
    $this->uris = $uris;
    parent::__construct($requestMethod, $uris, $body);
    $this->einnahmenRepository = new EinnahmenRepository();
    $this->aufzeichnungenRepository = new AufzeichnungenRepository();
    $this->responseFactory = new ResponseFactory();
  }

  private function getNextEinnahme($list) {
    $wdayToday = date("N"); // weekday (1-7)
    for($i=0; $i<7;$i++) {
      foreach($list as $einnahme) {
        $wday = ($wdayToday + $i) % 7;
        if (in_array($wday, $einnahme->wochentage)) {
          if ($wday === $wdayToday) {
            $dt = new DateTime($einnahme->uhrzeit);
            $time = strtotime($dt->format('H:i')) - time();
            if ($time < 0) {
              continue;
            }
          }
          $einnahme->wochentage = [$wday];
          return $einnahme;
        }
      }
    }
  }


  protected function processGet(): Response
  {
    $result = $this->einnahmenRepository->findAll();
     // to display next einnahme
     if (sizeof($this->uris) > 2 && $this->uris[2] === 'next') {
      $item = $this->getNextEinnahme($result);
      $response = $this->responseFactory->getResponse(200, $item);
      return $response;
  }
  if (sizeof($this->uris) > 3 && $this->uris[2] !== 'next' && $this->uris[3] === 'aufzeichnung') {
    $id = intval($this->uris[2]);
    $result = $this->aufzeichnungenRepository->findAllByEinnahmeId($id);
    $response = $this->responseFactory->getResponse(200, $result);
  }
    $response = $this->responseFactory->getResponse(200, array('list' => $result));
    return $response;   
  }
  
  protected function processPost(): Response
  {
    $result = $this->einnahmenRepository->create($this->body);
    $response = $this->responseFactory->getResponse(200, $result);
    return $response;
  }

  protected function processPut(): Response
  {
    // required fields
    if (!isset($this->body) || !isset($this->body['name']) || !isset($this->body['farbe']) || !isset($this->body['form']) || !isset($this->body['uhrzeit']) || !isset($this->body['wochentage']) || !isset($this->body['id'])) {
      $response = $this->responseFactory->getResponse(400);
      return $response;
    }
    $result = $this->einnahmenRepository->change($this->body);
    $response = $this->responseFactory->getResponse(200, $result);
    return $response;
  }

  protected function processDelete(): Response
  {
    if (!isset($this->body) || !isset($this->body['id'])) {
      $response = $this->responseFactory->getResponse(400);
      return $response;
    }
    $id = $this->body['id'];
    $this->einnahmenRepository->delete($id);
    $response = $this->responseFactory->getResponse(200);
    return $response;

  }

}
?>