<?php
class Einnahme {
    public $id;
    public $name;
    public $farbe = null;
    public $tablettenform = null;
    public $uhrzeit;
    public $wochentage;

    public function __construct($params) {
        $this->id = $params['id'];
        $this->name = $params['name'];
        $this->farbe = $params['farbe'];
        $this->tablettenform = isset($params['tablettenform']) ? $params['tablettenform'] : null;
        $this->uhrzeit = $params['uhrzeit'];
        $this->melodie = $params['melodie'] ?? null;
        $this->wochentage = $params['wochentage'];
    }

    public function __toString() {
        $arr = array(
            'id' => $this->id,
            'name' => $this->name,
            'farbe' => $this->farbe,
            'tablettenform' => $this->tablettenform,
            'uhrzeit' => $this->uhrzeit,
            'wochentage' => $this->wochentage,
        );
        if (isset($this->melodie)) {
            $arr['melodie'] = $this->melodie;
        }
        return json_encode($arr);
    }
}

?>