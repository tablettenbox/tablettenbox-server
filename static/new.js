let selectedImg = null;
let selectedSound = "ALARMTON";

function onTablettenFormClick(e) {
    const id = e.target.dataset.id;
    selectedImg = id;
    const tablettenform = document.getElementById('tablettenform')
    const children = tablettenform.children;
    for(let i = 0; i<children.length; i++) {
        if (children[i].dataset.id === id) {
            children[i].classList.add("tablettenform_active");
        } else {
            children[i].classList.remove("tablettenform_active");
        }
    }

}
window.onload = function() {
const soundgroup = document.getElementById('sound-group');
soundgroup.onclick = function(e) {
    e.preventDefault();
    if (e.target.classList.contains('btn')) {
        selectedSound = e.target.dataset.key;
        const children = soundgroup.children;
        for(let i=0;i<children.length; i++) {
            if (children[i].dataset.key === selectedSound) {
                children[i].classList.add("active");
            } else {
                children[i].classList.remove("active");
            }
        }
    };
}

fetch('/api/tablettenform')
.then(resp => resp.json())
.then((resp) => {
    const tablettenform = document.getElementById('tablettenform')
    let innerHtml = '';
    for(let i=0;i<resp.length;i++) {
        const tablettenform =  resp[i];
        innerHtml = innerHtml + `<img class="tablettenform" type='image' data-id="${tablettenform.id}" id="tablettenform-${tablettenform.id}" src="${tablettenform.bild_url}">`
    }
    tablettenform.innerHTML = innerHtml;
    const list = document.getElementsByClassName('tablettenform');
    for(let i = 0; i < list.length; i++) {
        list[i].onclick = onTablettenFormClick;
    }
})
    const form = document.getElementById('form');
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        const name = document.getElementById('name').value;
        const farbe = document.getElementById('farbe').value;
        const wochentage = [];
        for(let i=0;i<6;i++) {
            if (document.getElementById('d' + i).checked) {
                wochentage.push(i);
            }
        }
        // check if all requried fields selected
        if (!selectedImg || !name || !farbe || wochentage.length === 0) {
            alert('Sie haben nicht alle Felder ausgefüllt');
            return;
        }
        const uhrzeit = document.getElementById('time').valueAsDate;
        const content = { 
            name,
            wochentage,
            uhrzeit: uhrzeit.toJSON().slice(0, -1),
            farbe,
            tablettenform_id: parseInt(selectedImg, 10),
            sound: selectedSound,
        };
        fetch('/api/einnahmen', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(content),
        }).then(() => {
            window.location.href = "/";
        })
    })
}