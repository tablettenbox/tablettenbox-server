<?php

include_once "src/repository/AbstractRepository.php";
include_once "src/model/Tablettenform.php";

class TablettenFormRepository extends AbstractRepository {

  public function findAll() {
    $query = $this->db->prepare("SELECT id, bild_url FROM Tablettenform");
    $arrayResult = $this->query($query);
    $result = [];
    foreach($arrayResult as $arrayObject) {
      $result[] = new Tablettenform ($arrayObject['id'], $arrayObject['bild_url']); 
    }
    return $arrayResult;
  }
}
?>