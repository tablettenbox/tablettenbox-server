function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function renderList(list) {
    document.getElementById('content').style.display = 'block';
    const div = document.getElementById('tabelle');
    let innerhtml;
if (!list || list.length ==  0) {
    innerhtml =  'Es steht keine Aufzeichnung zur Verügung'
    document.getElementById('header').style.display = 'none';
} else {
    innerhtml = list
        .map(einnahme => {
            return `
            <div class="row" id="einnahme-${einnahme.id}">
            <div class="col"> 
            ${einnahme.uhrzeit}
            </div>
            </div>`
        })
        .join('');
    }
    div.innerHTML += innerhtml;
}

window.onload = function() {
    const id = findGetParameter('id')
    fetch(`/api/einnahmen/${id}/aufzeichnung`)
        .then(resp => resp.json())
        .then((resp) => {
            renderList(resp.list);
        })
        .catch(err => {
            renderList([]);
        })
}

function deleteEinnahme(id) {
    fetch('/api/einnahmen', {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ id: id })
    })
        .then(() => {
            deleteEinnahmeFromDOM(id);
        });
}