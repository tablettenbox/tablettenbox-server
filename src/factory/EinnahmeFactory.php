<?php
include_once "src/model/Einnahme.php";
include_once "src/model/Tablettenform.php";

class EinnahmeFactory
{
    public function convertFromDatabase($value): Einnahme
    {
        $wochentage = [];
        //convert weekdays from int to array
        $bitmask = str_split(strrev(decbin($value['wochentage'])));
        foreach ($bitmask as $i => $val) {
            if ($val == 1) {
                $wochentage[] = 6 - $i;
            }
        }

        $tablettenform = isset($value['tablettenform_id']) ? new Tablettenform($value['tablettenform_id'], $value['bild_url']) : null;


        $params = array(
            'id' => $value['id'],
            'name' => $value['tabletten_name'],
            'farbe' => $value['farbe'],
            'uhrzeit' => $value['uhrzeit'],
            'wochentage' => $wochentage,
        );
        if (isset($value['melodie'])) {
            $params['melodie'] = $value['melodie'];
        }

        if (isset($tablettenform)) {
            $params['tablettenform'] = $tablettenform;
        }
        $einnahme = new Einnahme($params);

        return $einnahme;
    }
}
?>