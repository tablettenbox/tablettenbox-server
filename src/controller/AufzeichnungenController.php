<?php
include_once "src/controller/AbstractController.php";
include_once "src/factory/ResponseFactory.php";
include_once "src/repository/EinnahmenRepository.php";
include_once "src/repository/AufzeichnungenRepository.php";


class AufzeichnungenController extends AbstractController {
  private $body;
  private $responseFactory;
  private $einnahmenRepository;
  private $aufzeichnungenRepository;

  public function __construct(string $requestMethod, array $uris) 
  {
    $body = json_decode(file_get_contents('php://input'), true);
    $this->body = $body;
    $this->uris = $uris;
    $this->responseFactory = new ResponseFactory();
    parent::__construct($requestMethod, $uris, $body);
    $this->einnahmenRepository = new EinnahmenRepository();
    $this->aufzeichnungenRepository = new AufzeichnungenRepository();
  }

  protected function processGet(): Response
  {
    // for android
    if (sizeof($this->uris) > 2 && $this->uris[2] === 'create') { 
      return $this->processPost();
    }

    $response = $this->responseFactory->getResponse(404);
    return $response;
  }

  protected function processPost(): Response
  {
    $resp = array('result' => true);
    $response = $this->responseFactory->getResponse(200, $resp);
    $einnahmen = $this->einnahmenRepository->findAllNow(600);
    if (sizeof($einnahmen) > 0) {
      foreach($einnahmen as $einnahme) {
        $this->aufzeichnungenRepository->create($einnahme->id);
      }
      return $response;
    } 
    $this->aufzeichnungenRepository->create();

    return $response;
  }
  protected function processPut(): Response
  {
    $response = $this->responseFactory->getResponse(404);
    return  $response;
  }
  protected function processDelete(): Response
  {
    $response = $this->responseFactory->getResponse(404);
    return  $response;
  }
}
?>