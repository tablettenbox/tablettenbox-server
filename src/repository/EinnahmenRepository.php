<?php

include_once "src/model/Einnahme.php";
include_once "src/factory/EinnahmeFactory.php";
include_once "src/repository/AbstractRepository.php";

class EinnahmenRepository extends AbstractRepository
{
  public function create($props) {
    mysqli_report(MYSQLI_REPORT_ALL);
    $query = $this->db->prepare("INSERT INTO Einnahmen(tabletten_name, farbe, tablettenform_id, uhrzeit, wochentage, melodie) VALUES (?,?,?,?,?,?)");
    $wochentagesum = 0;
    // convert weekdays to int
    if (isset ($props['wochentage'])) {
        foreach ($props['wochentage'] as $val) {
        $wochentagesum += pow(2, 6 - $val);
      }
    }
    $query->bind_param('ssssds', $props['name'], $props['farbe'], $props['tablettenform_id'], $props['uhrzeit'], $wochentagesum, $props['sound']);
    $arrayResult = $this->query($query);
    return $arrayResult;
  }

  public function findAll() {
    $stmt = $this->db->prepare(
      'SELECT Einnahmen.id as id, Einnahmen.tabletten_name as tabletten_name, Einnahmen.farbe as farbe, Tablettenform.bild_url as bild_url, Tablettenform.id as tablettenform_id, Einnahmen.uhrzeit as uhrzeit, Einnahmen.wochentage as wochentage
       FROM Einnahmen
       INNER JOIN Tablettenform ON Einnahmen.tablettenform_id = Tablettenform.id
       ORDER BY wochentage DESC, uhrzeit'
    );
    $arrayResult = $this->query($stmt);

    $result = [];
    foreach($arrayResult as $arrayObject) {
      $result[] = (new EinnahmeFactory())->convertFromDatabase($arrayObject);
    }

    return $result;
  }

  public function findOne($id) {
    $query = $this->db->prepare(
      'SELECT id, tabletten_name, farbe, form, uhrzeit, wochentage FROM Einnahmen WHERE id = ?'
    );
    $query->bind_param('d', $id);
    $arrayResult = $this->query($query);

    $one = reset($arrayResult) || null;

    return (new EinnahmeFactory())->convertFromDatabase($one);
  }

  /**
   * @param time period of time, s 
   * find all drugs to be taken in the moment with toleration +- (time) s
   */
  public function findAllNow($time = 60) {
    $today = date("Y-m-d");
    $number = date('N', strtotime($today));
    $numberSql = pow(2, 7 - $number);
    $stmt = $this->db->prepare(
      "SELECT id, tabletten_name, farbe, uhrzeit, wochentage, melodie FROM Einnahmen WHERE abs(to_seconds(TIME(uhrzeit)) - to_seconds(TIME(NOW()))) <= $time AND wochentage & $numberSql"
    );
    $arrayResult = $this->query($stmt);

    $result = [];
    foreach($arrayResult as $arrayObject) {
      $result[] = (new EinnahmeFactory())->convertFromDatabase($arrayObject);
    }

    return $result;
  }

  public function change($body){
    $id = $body['id'];
    $query = $this->db->prepare('UPDATE Einnahmen SET tabletten_name = ?, farbe = ?, form = ?, uhrzeit = ?, wochentage = ?  WHERE id = ?');
    $wochentagesum = 0;
    if (isset ($props['wochentage'])) {
        foreach ($props['wochentage'] as $val) {
        $wochentagesum += pow(2, 6 - $val);
      }
    }
    $query->bind_param('sssddd', $body['name'], $body['farbe'], $body['form'], $body['uhrzeit'], $body['wochentage'], $id);

    $this->query($query);

    $result = $this->findOne($id);
    return $result;
  }

  public function delete($id){
    $query = $this->db->prepare('DELETE FROM Einnahmen WHERE id = ?');
    $query->bind_param('d', $id);
    
    $this->query($query);
  }
}
?>