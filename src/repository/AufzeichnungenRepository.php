<?php

include_once "src/repository/AbstractRepository.php";
include_once "src/factory/AufzeichnungFactory.php";

class AufzeichnungenRepository extends AbstractRepository
{
  public function create($einnahme_id = null) {
    $query = $this->db->prepare("INSERT INTO Aufzeichnungen(einnahme_id) VALUES (?)");
    $query->bind_param('d', $einnahme_id);
    $arrayResult = $this->query($query);
    return $arrayResult;
  }

  /**
   * select all drugs that were taken today
   */
  public function findAllByEinnahmeNow($einnahme) {
    $query = $this->db->prepare("SELECT id, uhrzeit, einnahme_id FROM Aufzeichnungen WHERE einnahme_id = ? AND DATE(uhrzeit) = CURRENT_DATE");
    $query->bind_param('d', $einnahme->id);
    $arrayResult = $this->query($query);
    $result = [];
    foreach($arrayResult as $arrayObject) {
      $result[] = (new AufzeichnungFactory())->convertFromDatabase($arrayObject, $einnahme);
    }
    return $arrayResult;
  }

  public function findAllByEinnahmeId($einnahmeId) {
    $query = $this->db->prepare("SELECT id, uhrzeit, einnahme_id FROM Aufzeichnungen WHERE einnahme_id = ?");
    $query->bind_param('d', $einnahmeId);
    $arrayResult = $this->query($query);
    $result = [];
    foreach($arrayResult as $arrayObject) {
      $result[] = (new AufzeichnungFactory())->convertFromDatabase($arrayObject, $einnahmeId);
    }
    return $arrayResult;
  }
}
?>