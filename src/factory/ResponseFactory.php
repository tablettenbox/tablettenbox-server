<?php
class Response {
  public string $header;
  public ?string $body = null;
  public int $code;

  public function __construct(string $header, ?string $body = null, int $code) {
    $this->header = $header;
    $this->body = $body; 
    $this->code = $code;
  }
}

class ResponseFactory {
  public function getResponse(int $code, $body = null):Response {
    switch ($code) {
      case 200:
        $header = "HTTP/1.1 200 OK";
        $bodyString = $body ? json_encode($body) : null;
        return new Response($header, $bodyString, 200);
      case 404:
        $header = "HTTP/1.1 404 Not Found";
        return new Response($header, null, 404);
      case 400:
      default:
        $header = "HTTP/1.1 400 Bad Request";
        return new Response($header, null, 400);
      }
    }
}