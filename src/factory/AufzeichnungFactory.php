<?php
include_once "src/model/Aufzeichnung.php";

class AufzeichnungFactory
{
    public function convertFromDatabase($value, $einnahme): Aufzeichnung
    {

        $params = array(
            'id' => $value['id'],
            'uhrzeit' => $value['uhrzeit'],
            'einnahme' => $einnahme,
        );

        $aufzeichnung = new Aufzeichnung($params);

        return $aufzeichnung;
    }
}
?>