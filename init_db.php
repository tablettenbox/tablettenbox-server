<?php
  include "config/database.php";
  $mysqli = new mysqli(Database::HOST, Database::USERNAME, Database::PASSWORD); 
  $dbname = Database::DBNAME;

  function createDb($mysqli, $dbname) {
    $db = $mysqli->select_db($dbname);
    $sql = 'CREATE DATABASE ' . $dbname;
    if ($db) {
      return;
    }
    if ($mysqli->query($sql)) {
      echo 'Datenbank ' . $dbname . " wurde angelegt.\n"; 
    } else {
      echo 'Bei der Erstellung einer Datenbank ist ein Fehler aufgetreten: ' . $mysqli->error . "\n";
    }  
  }

  function createTable($mysqli, $sql) {
    if ($mysqli->query($sql) === TRUE) {
      echo "Tabelle Einnahmen ist erstellt\n";
      return true;
    } else {
      echo "Bei der Erstellung einer Tabelle ist ein Fehler aufgetreten:  " . $mysqli->error;
      return false;
    }
  }

  function createEinnahmeTable($mysqli) {
    $sql = "CREATE TABLE Einnahmen (
      id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      tabletten_name VARCHAR(30),
      farbe VARCHAR(30),
      melodie VARCHAR(30),
      tablettenform_id INT(3) UNSIGNED,
      uhrzeit TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      wochentage INT(3) UNSIGNED,
      FOREIGN KEY (tablettenform_id) 
        REFERENCES Tablettenform(id)
        ON DELETE CASCADE
    )";
    createTable($mysqli, $sql);
  }

  function createAufzeichnungenTable($mysqli) {
    $sql = "CREATE TABLE Aufzeichnungen (
      id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      uhrzeit TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      einnahme_id INT(6) UNSIGNED DEFAULT NULL,
      FOREIGN KEY (einnahme_id) 
        REFERENCES Einnahmen(id)
        ON DELETE CASCADE
    )";
    createTable($mysqli, $sql);
  }

  function creatTablettenformTable($mysqli) {
    $sql = "CREATE TABLE Tablettenform (
      id INT(3)  UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      bild_url VARCHAR (30)
    )";
    if (createTable($mysqli, $sql)) {
      insertImagesIntoTablettenformTable($mysqli);
    };
  }

  function insertImagesIntoTablettenformTable($mysqli) {
  $sql = "INSERT INTO Tablettenform (bild_url) 
          VALUES ('/images/tablettenform.jpg'),
                 ('/images/tablettenform2.jpg'),
                 ('/images/tablettenform3.jpg'),
                 ('/images/tablettenform4.jpg'),
                 ('/images/tablettenform9.jpg'),
                 ('/images/tablettenform7.jpg'),
                 ('/images/tablettenform8.jpg'),
                 ('/images/tablettenform6.jpg')              
  ";
  if ($mysqli->query($sql) === TRUE) {
    echo "Bilder der Tabletten wurden erfolgreich hinzugefügt\n";
  } else {
    echo "Bei dem Hinzufügen der Bilder ist ein Fehler aufgetreten:  " . $mysqli->error;
  }

  }

  $sql = 'DROP DATABASE ' . $dbname;
  $mysqli->query($sql);
  createDb($mysqli, $dbname);
  $mysqli->select_db($dbname);
  creatTablettenformTable($mysqli);
  createEinnahmeTable($mysqli);
  createAufzeichnungenTable($mysqli);
  $mysqli->close();
?>