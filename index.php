<?php
include_once "src/controller/EinnahmenController.php";
include_once "src/controller/StatusController.php";
include_once "src/controller/AufzeichnungenController.php";
include_once "src/controller/TablettenFormController.php";
include_once "src/factory/ResponseFactory.php";


$repsonseFactory = new ResponseFactory();
$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uris = array_values(array_filter(explode( '/', $uri), 'strlen'));
// URL router
if (sizeof($uris) > 0 && $uris[0] === 'api') {
  switch($uris[1]) {
    case 'einnahmen':
      $einnahmenController = new EinnahmenController($_SERVER["REQUEST_METHOD"], $uris);
      $einnahmenController->processRequest();
      break;
    case 'status':
      $statusController = new StatusController($_SERVER["REQUEST_METHOD"], $uris);
      $statusController->processRequest();
      break;
    case 'aufzeichnung':
      $aufzeichnungenController = new AufzeichnungenController($_SERVER["REQUEST_METHOD"], $uris);
      $aufzeichnungenController->processRequest();
      break;
    case 'tablettenform':
      $tablettenformController = new TablettenformController($_SERVER["REQUEST_METHOD"], $uris);
      $tablettenformController->processRequest();
      break;
    default:
      $response = $repsonseFactory->getResponse(400);
      header($response->header, true, 400);
      break;
  }
  // if that's not an API Request, display HTML
} else {
  if (sizeof($uris) == 0) {
    echo file_get_contents('html/index.html');
    return;
  }
  switch($uris[0]) {
    case 'new':
      echo file_get_contents('html/new.html');
      break;
    case 'verlauf':
      echo file_get_contents('html/statistik.html');
      break;
    default:
      echo file_get_contents('html/index.html');
  }
  
}
