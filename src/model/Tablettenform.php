<?php 

class Tablettenform {
    public $id;
    public $bildUrl;

    public function __construct($id, $bildUrl) {
        $this->id = $id;
        $this->bildUrl = $bildUrl;
    }

    public function __toString() {
        $arr = array(
            'id' => $this->id,
            'bildUrl' => $this->bildUrl,   
        );
        return json_encode($arr);
    }
}

?>