<?php
include_once "src/repository/TablettenFormRepository.php";
include_once "src/controller/AbstractController.php";



class TablettenformController extends AbstractController {
  private TablettenFormRepository $tablettenformRepository;
  private ResponseFactory $responseFactory;
  private $body;

  public function __construct(string $requestMethod, array $uris) 
  {
    $body = json_decode(file_get_contents('php://input'), true);
    $this->body = $body;
    parent::__construct($requestMethod, $uris, $body);
    $this->tablettenformRepository = new TablettenFormRepository();
    $this->responseFactory = new ResponseFactory();
  }


  protected function processGet(): Response
  {
    $result = $this->tablettenformRepository->findAll();
    $response = $this->responseFactory->getResponse(200, $result);
    return $response;   
  }
  
  protected function processPost(): Response
  {    
    $response = $this->responseFactory->getResponse(404);
    return $response;
  }

  protected function processPut(): Response
  {
    $response = $this->responseFactory->getResponse(404);
    return $response;
  }

  protected function processDelete(): Response
  {
    $response = $this->responseFactory->getResponse(404);
    return $response;
  }

}
?>