<?php
include_once "src/repository/EinnahmenRepository.php";
include_once "src/repository/AufzeichnungenRepository.php";
include_once "src/controller/AbstractController.php";
include_once "src/factory/ResponseFactory.php";


class StatusController extends AbstractController {
  private $body;
  private $responseFactory;
  private $einnahmenRepository;
  private $aufzeichnungenRepository;

  public function __construct(string $requestMethod, array $uris) 
  {
    $body = json_decode(file_get_contents('php://input'), true);
    $this->body = $body;
    $this->responseFactory = new ResponseFactory();
    parent::__construct($requestMethod, $uris, $body);
    $this->einnahmenRepository = new EinnahmenRepository();
    $this->aufzeichnungenRepository = new AufzeichnungenRepository();
  }

  public function getStatus() 
  {
    $einnahmen = $this->einnahmenRepository->findAllNow();
    $status = 'OK';
    $wday = intval(date("N")); // weekday (0-7)

    if (sizeof($einnahmen) > 0) {
    // count of all drugs to be taken now
    $einnahmen = array_filter($einnahmen, function($einnahme) {
      // check if it was already taken
      $aufzeichnungen = $this->aufzeichnungenRepository->findAllByEinnahmeNow($einnahme);
      return sizeof($aufzeichnungen) == 0;
    });
    $einnahmenCount = sizeof($einnahmen);
      if ($einnahmenCount > 0) {
        $status = 'ALARM';
      }
    }
    $res = array(
        'status' => $status,
        'wday' => $wday,
    );
    if (isset($einnahmen[0])) {
      $res['sound'] = $einnahmen[0]->melodie;
    }
    return $res;
  }
  protected function processGet(): Response
  {
    $result = $this->getStatus();  
    $response = $this->responseFactory->getResponse(200, $result);
    return $response;
  }

  protected function processPost(): Response
  {
    $response = $this->responseFactory->getResponse(404);
    return  $response;
  }
  protected function processPut(): Response
  {
    $response = $this->responseFactory->getResponse(404);
    return  $response;
  }
  protected function processDelete(): Response
  {
    $response = $this->responseFactory->getResponse(404);
    return  $response;
  }
}
?>