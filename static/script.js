// weekdays to display
const tagen = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'];
/**
 * hide an element from DOM that has been already deleted
 * @param {Number} id id of Einnahme that should be deleted 
 */
function deleteEinnahmeFromDOM(id) {
    const element = document.getElementById(`einnahme-${id}`);
    element.style.opacity = 0;
    setTimeout(() => {
        element.style.display = 'none';
    }, 1000);
}
/**
 * format time (9:9 -> 09:09, 10:9 -> 10:09)
 * @param {Date} date date object
 * @returns {String} formatted time
 */
function formatTime(date) {
    const h = date.getHours();
    const m = date.getMinutes();
    let res = '';
    if (h < 10) {
        res = '0' + h.toString();
    } else {
        res = h.toString();
    }
    res += ':';
    if (m < 10) {
        res += ('0' + m.toString())
    } else {
        res += m.toString();
    }
    return res;
}
/**
 * display the list in DOM
 * @param {Array} list list of Einnahmen
 */

function renderList(list) {
    document.getElementById('content').style.display = 'block';
    const div = document.getElementById('tabelle');
    const innerhtml = list
        .map(einnahme => {
            // convert weekdays to readable text ([0, 2] -> [Mo, Mi])
            const weekdays = tagen
                .filter((_, j) => einnahme.wochentage.includes(j))
                .join(', ');
            const date = new Date(einnahme.uhrzeit);
            const time = formatTime(date);
            return `
            <div class="row" id="einnahme-${einnahme.id}">
            <div class="col">
            <img src="${einnahme.tablettenform.bildUrl}" width="100">
            </div>
            <div class="col"> 
            ${einnahme.name}
            </div>
            <div class="col"> 
            ${einnahme.farbe}
            </div>
            <div class="col"> 
            ${weekdays}
            </div>
            <div class="col"> 
            ${time}
            </div>
            <div class="col-2"> 
            <a href="/verlauf?id=${einnahme.id}" class="edit">
            <i class="bi bi-bar-chart-line"></i>
            </a>
            <button type="button" onclick="javascript:deleteEinnahme(${einnahme.id})" class="close" aria-label="Close">
            <i class="bi bi-trash"></i>
            </button>
            </div>
            </div>`
        })
        .join('');
        
    div.innerHTML += innerhtml;
}

window.onload = function() {
    fetch('/api/einnahmen')
        .then(resp => resp.json())
        .then((resp) => {
            renderList(resp['list']);
        })
}

function deleteEinnahme(id) {
    fetch('/api/einnahmen', {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ id: id })
    })
        .then(() => {
            deleteEinnahmeFromDOM(id);
        });
}