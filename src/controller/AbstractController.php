<?php
include_once "src/factory/ResponseFactory.php";

abstract class AbstractController {
    private $requestMethod;
    private ResponseFactory $responseFactory;
    private $uris;
    private $body;

    abstract protected function processGet():Response;
    abstract protected function processPost():Response;
    abstract protected function processPut(): Response;
    abstract protected function processDelete(): Response;

    
    public function __construct(string $requestMethod, array $uris, $body = null) {
        $this->requestMethod = $requestMethod;
        $this->responseFactory = new ResponseFactory();
        $this->uris = $uris;
        $this->body = $body;
    }

    private function getResponse(): Response {
        switch ($this->requestMethod) {
            case 'GET':
                return $this->processGet();
                break;
            case 'POST':
                return $this->processPost();
                break;
            case 'PUT':
                return $this->processPut();
                break;
            case 'DELETE':
                return $this->processDelete();
                break;
            default:
                return $this->responseFactory->getResponse(400);
        }

    }

    public function processRequest() {
        $response = $this->getResponse();
        
        header($response->header, true, $response->code);
        header('Content-Type: application/json');
        if ($response && $response->body) {
            die($response->body);
        }
    }
    
}