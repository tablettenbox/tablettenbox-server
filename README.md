# Tablettenbox (PHP-Server-App)

- [Tablettenbox (PHP-Server-App)](#tablettenbox-php-server-app)
  - [Requirements](#requirements)
  - [Getting Starting](#getting-starting)
    - [DB Setup](#db-setup)
    - [User interface](#user-interface)
## Requirements

 - PHP 7+
 - MySQL / MariaDB
 - Apache or any webserver
## Getting Starting

This tutorial assumes that you have already installed everything listed above. The fastest to get it may be using XAMPP or WAMP. 
### DB Setup

1. You have to configure your MySQL credentials in `config/database.php`
2. You have to start your Apache server and execute a script located [localhost/init_db.php](http://localhost/init_db.php)
3. DB Setup is completed. Do not forget to remove `init_db.php` due to security reasons.

### User interface

Go to [localhost/new](http://localhost/new) and try to add any items. It should be appeared on the homepage. If everything was configured the right way, a drug box should be connected with this application.